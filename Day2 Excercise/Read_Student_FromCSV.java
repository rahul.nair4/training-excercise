package Day2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Read_Student_FromCSV {

	public static void main(String[] args) throws Exception {
		FileReader file = new FileReader("D:\\Eclipse-workspace\\Excercises\\src\\Day2\\student.csv");
		BufferedReader read = new BufferedReader(file);
		String Line = null;
		ArrayList<Student> student_details_arraylist = new ArrayList<>();

		while ((Line = read.readLine()) != null) {
			String[] studentData = Line.split(",");
			/* int id, String name, String class_id, int marks, String gender, int age */
			Student studentdetails = new Student(Integer.parseInt(studentData[0]),
					studentData[1], studentData[2], Integer.parseInt(studentData[3]), studentData[4],
					Integer.parseInt(studentData[5]));

			student_details_arraylist.add(studentdetails);

		}

		/*
		 * for (int i = 0; i < student_details_arraylist.size(); i++) {
		 * Student_Data_Getter student_get_details = student_details_arraylist.get(i); }
		 */
		System.out.println(student_details_arraylist);

	}

}
