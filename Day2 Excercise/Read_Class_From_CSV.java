package Day2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class Read_Class_From_CSV {

	public static void main(String[] args) throws Exception {
		FileReader file = new FileReader("D:\\Eclipse-workspace\\Excercises\\src\\Day2\\class.csv");
		BufferedReader read = new BufferedReader(file);
		String Line = null;
		ArrayList<Class_section> class_details_arraylist = new ArrayList<>();

		while ((Line = read.readLine()) != null) {
			String[] classData = Line.split(",");
			/* int id, String name, String class_id, int marks, String gender, int age */
			Class_section classdetails = new Class_section(Integer.parseInt(classData[0]), classData[1]);

			class_details_arraylist.add(classdetails);

		}

		System.out.println(class_details_arraylist);

	}

}
