package Day2;

/*Class
id | name
1  | A
2  | B
3  | C
4  | D
*/
public class Class_section {

	private int id;
	private String name;

	public Class_section(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
