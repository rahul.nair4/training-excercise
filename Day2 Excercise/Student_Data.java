package Day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class Student_Data {

	public static void main(String[] args) {
		List<Class_section> classdata = new ArrayList<>(); // id | name

		classdata.add(new Class_section(1, "A"));
		classdata.add(new Class_section(2, "B"));
		classdata.add(new Class_section(3, "C"));
		classdata.add(new Class_section(4, "D"));

		/* System.out.println(classdata.get(3)); */

		List<Student> studentdata = new ArrayList<>(); // id | name | class_id | marks | gender | age

		studentdata.add(new Student(1, "stud1", "A", 88, "F", 10));
		studentdata.add(new Student(2, "stud2", "A", 70, "F", 11));
		studentdata.add(new Student(3, "stud3", "B", 88, "M", 22));
		studentdata.add(new Student(4, "stud4", "B", 55, "M", 33));
		studentdata.add(new Student(5, "stud5", "A", 30, "F", 44));
		studentdata.add(new Student(6, "stud6", "C", 30, "F", 33));
		studentdata.add(new Student(7, "stud6", "C", 10, "F", 22));
		studentdata.add(new Student(8, "stud6", "C", 0, "M", 11));

		List<Address> addressdata = new ArrayList<>(); // id | pin_code | city | student_id

		addressdata.add(new Address(1, 452002, "indore", 1));
		addressdata.add(new Address(2, 422002, "indore", 1));
		addressdata.add(new Address(3, 442002, "indore", 2));
		addressdata.add(new Address(4, 462002, "delhi", 3));
		addressdata.add(new Address(5, 472002, "indore", 4));
		addressdata.add(new Address(6, 452002, "indore", 5));
		addressdata.add(new Address(7, 452002, "delhi", 5));
		addressdata.add(new Address(8, 482002, "mumbai", 6));
		addressdata.add(new Address(9, 482002, "bhopal", 7));
		addressdata.add(new Address(10, 482002, "indore", 8));

		
		  List<Integer> add_data = new ArrayList<>() ; 
		  for(Address pin:addressdata) {
		  if(pin.getPin_code()==482002) { 
			  add_data.add(pin.getStudent_id()); 
			  } 
		  }
		 

		// Question:-1
		// filltered by pincode
		
		  addressdata.stream().filter(X->X.getPin_code()==482002)
		  .forEach(X->studentdata.stream().filter(y->X.getStudent_id()==y.getId())
		  .forEach(z->System.out.println(z)));
		  System.out.println("####################################################################################");
		 
		// filterby gender

		
		  System.out.println("\n\nfiltered by Female gender\n");
		  addressdata.stream().filter(X->X.getPin_code()==482002)
		  .forEach(X->studentdata.stream().filter(y->X.getStudent_id()==y.getId())
		  .filter(z->z.getGender().equals("F")).forEach(q->System.out.println(q)));
		  
		  System.out.println("\n\nfiltered by Male gender\n");
		  addressdata.stream().filter(X->X.getPin_code()==482002)
		  .forEach(X->studentdata.stream().filter(y->X.getStudent_id()==y.getId())
		  .filter(z->z.getGender().equals("M")).forEach(g->System.out.println(g)));
		 

		// filter by age
		
		  System.out.println("\n\nfiltered by Age \n");
		  addressdata.stream().filter(X->X.getPin_code()==482002)
		  .forEach(X->studentdata.stream().filter(y->X.getStudent_id()==y.getId())
		  .filter(z->z.getAge()<=25).forEach(g->System.out.println(g)));
		 

		// filter by class
		
		  System.out.println("\n\nfiltered by class \n");
		  addressdata.stream().filter(X->X.getPin_code()==482002)
		  .forEach(X->studentdata.stream().filter(y->X.getStudent_id()==y.getId())
				  .filter(z->z.getClass_id()=="A").forEach(g->System.out.println(g)));//y  kpass studen //pin code checked thenreturened
		  System.out.println("####################################################################################");
		 

		/*
		 * Question 2 filter by city
		 */

		
		  addressdata.stream().filter(x -> x.getCity() == "indore") .forEach(x
		  ->studentdata.stream().filter(y->x.getStudent_id()==y.getId()).forEach(d->
		  System.out.println(d)));
		 
		// filter by gender
		
		  System.out.println("\n\nfiltered by Male gender\n");
		  addressdata.stream().filter(x -> x.getCity() == "indore") .forEach(x ->
		  studentdata.stream().filter(y -> x.getStudent_id() == y.getId()) .filter(z ->
		  z.getGender() == "M").forEach(g -> System.out.println(g)));
		  
		  System.out.println("\n\nfiltered by Female gender\n");
		  addressdata.stream().filter(x -> x.getCity() == "indore") .forEach(x ->
		  studentdata.stream().filter(y -> x.getStudent_id() == y.getId()) .filter(z ->
		  z.getGender() == "F").forEach(g -> System.out.println(g)));
		  System.out.println("####################################################################################");
		 

		/*
		 * Question 3 
		 */

		
		  Collections.sort(studentdata, (x, y) -> y.getMarks() - x.getMarks());
		  
		  int rank = 1, marks = 0; for (Student student : studentdata) { if (rank == 1
		  && marks == 0) { System.out.println(student.getName() + "||Marks:" +
		  student.getMarks() + "||rank:" + rank); marks = student.getMarks(); } else if
		  (rank < 3) { if (marks == student.getMarks()) {
		  System.out.println(student.getName() + "||Marks:" + student.getMarks() +
		  "||rank:" + rank); marks = student.getMarks(); } else { rank++;
		  System.out.println(student.getName() + "||Marks:" + student.getMarks() +
		  "||rank:" + rank); marks = student.getMarks();
		  
		  } } else if (rank == 3) {
		  System.out.println("----------------------------------------------------");
		  
		  } if (student.getMarks() < 50) { System.out.print("Failed Students: ");
		  System.out.println(student.getName() + "||Marks:" + student.getMarks()); } }
		  System.out.println("####################################################################################");
		 

		/*
		 * Question 4
		 */
		System.out.println("----------------------Passed Students>50-----------------------------");
		 List <Student> passed = studentdata.stream().filter(x->x.getMarks()>50).collect(Collectors.toList());
		 System.out.println("----Passed Students----");
		 passed.forEach(System.out::println);
		System.out.println("####################################################################################");

		/*
		 * Question 5
		 */
		System.out.println("----------------------Failed Students<50-----------------------------");
			  List <Student> failed
			  =studentdata.stream().filter(x->x.getMarks()<50).collect(Collectors.toList())
			  ; System.out.println("\n----Failed Students----");
			  failed.forEach(System.out::println);
			  
			  studentdata.stream().filter(x -> x.getGender().equals("M")).forEach(f
			  ->System.out.println(f));
		System.out.println("####################################################################################");

		/*
		 * Question 6
		 */
		System.out.println("----------------------Students of class A-----------------------------");
		  List <Student>
		  ClassA=studentdata.stream().filter(x->x.getClass_id()=="A").collect(
		  Collectors.toList());
		  ClassA.forEach(System.out::println);
		 
		ClassA.stream().filter(x -> x.getGender() == "M").forEach(y -> System.out.println(y));
		System.out.println("####################################################################################");

		/*
		 * Question 8
		 */
		System.out.println("----------------------Failed students > 20 age-----------------------------");
		List <Student> Student_age =studentdata.stream().filter(x->x.getAge()>20).collect(Collectors.toList());
		System.out.println("Failed Students");
		Student_age.forEach(System.out::println);
		System.out.println("####################################################################################");

		/*
		 * Question 9 10 
		 */
		System.out.println("----------------------Delete_Student_Adress_Class-----------------------------");
		int student_id = 1;
		String student_class_id = null;
		ListIterator<Student> student_delete = studentdata.listIterator();
		Student student = null;
		while (student_delete.hasNext()) {
			student = student_delete.next();
			if (student.getId() == student_id) {
				student_class_id = student.getClass_id();
				student_delete.remove();
			}
		}

		addressdata = addressdata.stream().filter(x -> !(x.getStudent_id() == 1)).collect(Collectors.toList());
		addressdata.forEach(System.out::println);

		ListIterator<Class_section> class_delete = classdata.listIterator();
		Class_section class_name = null;
		while (class_delete.hasNext()) {
			class_name = class_delete.next();
			if (class_name.getName().equals(student_class_id)) {
				class_delete.remove();
			}
		}

		/*
		 * Question 11
		 */
		System.out.println("####################################################################################");
		List <Student> Female_student=studentdata.stream().filter(x -> x.getGender().equals("F")).collect(Collectors.toList());
		Female_student.forEach(System.out::println);
		System.out.println("----------------------Female_student_paginate-----------------------------");
		
		List <Student> Female_student_paginate=Female_student.subList(0, 3);
		Female_student_paginate.forEach(System.out::println);
		
		System.out.println("-------------------------Female_student_ordername--------------------------");
		Collections.sort(Female_student,(x,y)->x.getName().compareTo(y.getName()));
		
		List <Student> Female_student_ordername=Female_student.subList(0, 3);
		Female_student_ordername.forEach(System.out::println);
		
		System.out.println("-------------------------Female_student_ordermarks--------------------------");
		Collections.sort(Female_student,(x,y)->y.getMarks()-x.getMarks());
		Female_student.forEach(System.out::println);
		
		
		

		

	}

}
