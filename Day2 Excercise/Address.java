package Day2;

/*Address
id | pin_code | city | student_id
1  | 452002| indore | 1
2  | 422002  | delhi | 1
3  | 442002  | indore | 2
4  | 462002  | delhi | 3
5  | 472002  | indore | 4
6  | 452002  | indore | 5
7  | 452002  | delhi | 5
8  | 482002  | mumbai | 6
9  | 482002  | bhopal | 7
10 | 482002 | indore | 8
*/

public class Address {

	private int id;
	private int pin_code;
	private String city;
	private int student_id;
	
	public Address(int id, int pin_code, String city, int student_id) {
		this.id = id;
		this.pin_code = pin_code;
		this.city = city;
		this.student_id = student_id;
	}

	public int getId() {
		return id;
	}

	public int getPin_code() {
		return pin_code;
	}

	public String getCity() {
		return city;
	}

	public int getStudent_id() {
		return student_id;
	}

	@Override
	public String toString() {
		return "\nAddress [id=" + id + ", pin_code=" + pin_code + ", city=" + city + ", student_id=" + student_id + "]\n";
	}
	
	
	
}
