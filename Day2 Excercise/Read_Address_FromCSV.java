package Day2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class Read_Address_FromCSV {

	public static void main(String[] args) throws Exception {
		FileReader file = new FileReader("D:\\Eclipse-workspace\\Excercises\\src\\Day2\\address.csv");
		BufferedReader read = new BufferedReader(file);
		String Line = null;
		ArrayList<Address> address_details_arraylist = new ArrayList<>();

		while ((Line = read.readLine()) != null) {
			String[] addressData = Line.split(",");
			/* int id, String name, String class_id, int marks, String gender, int age */
			Address addressdetails = new Address(Integer.parseInt(addressData[0]),Integer.parseInt(addressData[1]),
					addressData[2],Integer.parseInt(addressData[3]));

			address_details_arraylist.add(addressdetails);

		}

		System.out.println(address_details_arraylist);

	}

}
