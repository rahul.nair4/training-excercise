package Day2;

/*public String toString()
		{
			return 
		}
1 | stud1 | A       | 88 | F | 10
2 | stud2 | A       | 70 | F | 11
3 | stud3 | B       | 88 | M | 22
4 | stud4 | B       | 55 | M | 33
5 | stud5 | A       | 30 | F | 44
6 | stud6 | C       | 30 | F | 33
7 | stud6 | C       | 10 | F | 22
8 | stud6 | C       | 0 | M | 11*/

public class Student {

	@Override
	public String toString() {
		return "Student Data: [id=" + id + ", name=" + name + ", class_id=" + class_id + ", marks=" + marks + ", gender="
				+ gender + ", age=" + age + "]";
	}

	private int id;
	private String name;
	private String class_id;
	private int marks;
	private String gender;
	private int age;
	
	public Student(int id, String name, String class_id, int marks, String gender, int age) {
		this.id = id;
		this.name = name;
		this.class_id = class_id;
		this.marks = marks;
		this.gender = gender;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getClass_id() {
		return class_id;
	}

	public int getMarks() {
		return marks;
	}

	public String getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}
	
	

}
