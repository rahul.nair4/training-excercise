import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class PAthExist_Q23_Q30 {

	public static void main(String[] args) throws IOException {
		String path = "D:\\Eclipse-workspace\\Excercise Day1\\src\\Circle.java";
		File file = new File(path);

		if (file.exists()) {
			System.out.println("File \"" + file.getName() + "\" Exist in the given path.");

			Path path1 = Paths.get(path);
			byte[] filearray = Files.readAllBytes(path1);
			System.out.println(Arrays.toString(filearray));
		}

	}

}
