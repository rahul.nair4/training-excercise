import java.util.Scanner;

public class JavaConsole_Q28 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Name and Age: ");
		String name = scanner.next();
		int age = scanner.nextInt();

		System.out.println("Name:" + name + "\tAge:" + age);

	}

}
