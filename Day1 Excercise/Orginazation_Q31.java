import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class Orginazation_Q31 {

	public static void main(String[] args) throws ParseException {
		ArrayList<QrginazationData> EmployeData = new ArrayList<>();
		/*
		 * QrginazationData(String name, String gender, String department, Integer
		 * salary, Date joinDate, int age)
		 */

		QrginazationData org_data1 = new QrginazationData("Rahul", "Male", "Software Dev", 25000, 2021, 21);
		QrginazationData org_data2 = new QrginazationData("Shubham", "Male", "Software Dev", 15000, 2013, 22);
		QrginazationData org_data3 = new QrginazationData("Pradyum", "Male", "Web-Development", 17000, 2015, 20);
		QrginazationData org_data4 = new QrginazationData("Priya", "Female", "Web-Development", 15500, 2019, 19);
		QrginazationData org_data5 = new QrginazationData("Reshma", "Female", "Human Resource", 45000, 2010, 28);
		QrginazationData org_data6 = new QrginazationData("Ruhi", "Female", "Human Resource", 30000, 2026, 27);
		QrginazationData org_data7 = new QrginazationData("Ruhan", "Male", "Management", 50000, 200, 28);

		EmployeData.add(org_data1);
		EmployeData.add(org_data2);
		EmployeData.add(org_data3);
		EmployeData.add(org_data4);
		EmployeData.add(org_data5);
		EmployeData.add(org_data6);
		EmployeData.add(org_data7);

		while (true) {
			System.out.println("\t\t\t----Data Menu----");
			System.out.println("Enter 1 for :many male and female employees in the organization ");
			System.out.println("Enter 2 for :the name of all departments in the organization");
			System.out.println("Enter 3 for :the details of highest paid employee in the organization");
			System.out.println("Enter 4 for :the names of all employees who have joined after 2015");
			System.out.println("Enter 5 for : the average age of male and female employees");

			System.out.println("Enter 6 for :the number of employees in each department");
			System.out.println("Enter 7 for :the average salary of each department");
			System.out.println(
					"Enter 8 for : the details of youngest male employee in the Software development department");
			System.out.println(
					"Enter 9 for :how many male and female employees are there in the Software development and web development");
			System.out.println("Enter 10 for :the average salary and total salary of the whole organization");
			System.out.println(
					"Enter 11 for :the employees who are younger or equal to 25 years from those employees who are older than 25 years");
			System.out.println("Enter 12 for : Who is the oldest employee in the organization");

			System.out.println("Enter 13 for :  the names of all employees in each department");
			System.out.println("Enter 14 for : the average salary of male and female employees");
			System.out.println("Enter 15 for : Exit");
			System.out.println();
			Scanner scanner = new Scanner(System.in);

			int case_no = scanner.nextInt();
			switch (case_no) {
			case 1:
				int male = 0, female = 0;
				for (QrginazationData org_data : EmployeData) {
					if (org_data.getGender().equals("Male"))
						male++;
					else
						female++;
				}
				System.out.println("No. of Males:" + male + "\nNo. of FeMales:" + female);
				break;

			case 2:
				System.out.println("Employe_Name\t||\tGender\t||\tDepartment\t||\tsalary\t||\tJoinYear\t||\tAge");
				System.out.print(
						"_____________________________________________________________________________________________________________________________________________________________________________________________");
				for (QrginazationData org_data : EmployeData) {
					System.out.println(org_data.getName() + "\t\t||\t" + org_data.getGender() + "\t||\t"
							+ org_data.getDepartment() + "\t||\t" + org_data.getSalary() + "\t||\t"
							+ org_data.getJoinyear() + "\t\t||\t" + org_data.getAge());
				}
				break;

			case 3:
				Collections.sort(EmployeData, (x, y) -> y.getSalary() - x.getSalary());
				QrginazationData org_datahigestsalary;
				org_datahigestsalary = EmployeData.get(0);
				System.out.println(org_datahigestsalary.getName() + "\t\t||\t\t" + org_datahigestsalary.getGender()
						+ "\t\t||\t\t" + org_datahigestsalary.getDepartment() + "\t\t||\t\t"
						+ org_datahigestsalary.getSalary() + "\t\t||\t\t" + org_datahigestsalary.getJoinyear()
						+ "\t\t||\t\t" + org_datahigestsalary.getAge());
				break;

			case 4:
				Collections.sort(EmployeData, (x, y) -> x.getJoinyear() - y.getJoinyear());
				for (QrginazationData org_data : EmployeData) {
					if (org_data.getJoinyear() > 2015)
						System.out.println(org_data.getName() + "\t\t||\t\t" + org_data.getGender() + "\t\t||\t\t"
								+ org_data.getDepartment() + "\t\t||\t\t" + org_data.getSalary() + "\t\t||\t\t"
								+ org_data.getJoinyear() + "\t\t||\t\t" + org_data.getAge());
				}

				break;

			case 5:
				int maleAge = 0, femaleAge = 0, Malecount = 0, Femalecount = 0;
				for (QrginazationData org_data : EmployeData) {
					if (org_data.getGender().equals("Male")) {
						maleAge += org_data.getAge();
						Malecount++;
					} else {
						Femalecount++;
						femaleAge += org_data.getAge();
					}
				}
				System.out.println("the average age of male employees is " + maleAge / Malecount
						+ " and female employees is " + femaleAge / Femalecount);
				break;

			case 7:// the average salary of each department
				HashMap<String, ArrayList<Integer>> EmployeeDepartment = new HashMap();

				for (QrginazationData org_data : EmployeData) {
					if (EmployeeDepartment.containsKey(org_data.getDepartment())) {
						ArrayList<Integer> list = EmployeeDepartment.get(org_data.getDepartment());

						int count = list.get(0);
						int salarysum = list.get(1);
						list.add(0, ++count);
						list.add(1, salarysum + org_data.getSalary());

						EmployeeDepartment.put(org_data.getDepartment(), list);

					} else {
						ArrayList<Integer> arraycount = new ArrayList<>();
						arraycount.add(1);
						arraycount.add(org_data.getSalary());
						EmployeeDepartment.put(org_data.getDepartment(), arraycount);
					}

				}
				EmployeeDepartment.forEach((x, y) -> System.out.println(x + "\t" + (y.get(1) / y.get(0))));

			case 6:// the number of employees in each department
				int Employee = 0;
				HashMap<String, Integer> EmployeeDepartment1 = new HashMap();

				for (QrginazationData org_data : EmployeData) {
					if (EmployeeDepartment1.containsKey(org_data.getDepartment())) {
						Employee = EmployeeDepartment1.get(org_data.getDepartment());
						EmployeeDepartment1.put(org_data.getDepartment(), ++Employee);

					} else {
						EmployeeDepartment1.put(org_data.getDepartment(), 1);
					}

				}
				EmployeeDepartment1.forEach((x, y) -> System.out.println(x + "\t" + y));

			case 8:// the details of youngest male employee in the Software development department
				Collections.sort(EmployeData, (x, y) -> x.getAge() - y.getAge());
				QrginazationData org_dataYoungest;
				org_dataYoungest = EmployeData.get(0);
				System.out.println(org_dataYoungest.getName() + "\t||\t" + org_dataYoungest.getGender() + "\t||\t"
						+ org_dataYoungest.getDepartment() + "\t||\t" + org_dataYoungest.getSalary() + "\t||\t"
						+ org_dataYoungest.getJoinyear() + "\t||\t" + org_dataYoungest.getAge());

				break;

			case 9:// :how many male and female employees are there in the Software development and
					// web development"
				int malesoftDep = 0, malewebDep = 0, femalesoftDep = 0, femalewebDep = 0;
				for (QrginazationData org_data : EmployeData) {
					if (org_data.getDepartment().equals("Software Dev") && org_data.getGender().equals("Male"))
						malesoftDep++;
					if (org_data.getDepartment().equals("Web-Development") && org_data.getGender().equals("Male"))
						malewebDep++;

					if (org_data.getDepartment().equals("Software Dev") && org_data.getGender().equals("Female"))
						femalesoftDep++;
					if (org_data.getDepartment().equals("Web-Development") && org_data.getGender().equals("Female"))
						femalewebDep++;
				}
				System.out.println("No. of Males in Software development :" + malesoftDep
						+ "\nNo. of FeMales in Software development :" + femalesoftDep);
				System.out.println("No. of Males in web development :" + malewebDep
						+ "\nNo. of FeMales in web development :" + femalewebDep);
				break;

			case 10:// the average salary and total salary of the whole organization
				int totalsalarycount = 0, totalsalary = 0;
				for (QrginazationData org_data : EmployeData) {
					totalsalarycount++;
					totalsalary += org_data.getSalary();
				}
				System.out.println("the average salary is " + totalsalary / totalsalarycount + " and total salary is "
						+ totalsalary + " of the whole organization");

				break;

			case 11:// the employees who are younger or equal to 25 years from those employees who
					// are older than 25 years
				System.out.println("The employees who are younger or equal to 25 years");
				for (QrginazationData org_data : EmployeData) {
					if (org_data.getAge() <= 25)
						System.out.println(org_data.getName() + "\t" + org_data.getAge());
				}
				System.out.println("\nThe employees who are Elder to 25 years");
				for (QrginazationData org_data : EmployeData) {
					if (org_data.getAge() > 25)
						System.out.println(org_data.getName() + "\t" + org_data.getAge());
				}
				break;
			case 12:
				Collections.sort(EmployeData, (x, y) -> y.getAge() - x.getAge());
				QrginazationData org_dataOldest;
				org_dataOldest = EmployeData.get(0);
				System.out.println(org_dataOldest.getName() + "\t||\t" + org_dataOldest.getDepartment() + "\t||\t"
						+ org_dataOldest.getJoinyear() + "\t||\t" + org_dataOldest.getAge());
				break;

			case 13:// the names of all employees in each department
				for (QrginazationData org_data : EmployeData) {
					System.out.println(org_data.getName() + "\t||\t" + org_data.getDepartment());
				}
				break;

			case 14:
				int malesalary = 0, femalesalary = 0, malecount = 0, femalecount = 0;
				for (QrginazationData org_data : EmployeData) {
					if (org_data.getGender().equals("Male")) {
						malesalary += org_data.getSalary();
						malecount++;
					} else {
						femalecount++;
						femalesalary += org_data.getSalary();
					}
				}
				System.out.println("the average Salary of male employees is " + malesalary / malecount
						+ " and female employees is " + femalesalary / femalecount);
				break;

			}
		}

	}

}
