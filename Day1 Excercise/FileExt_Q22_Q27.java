import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FileExt_Q22_Q27 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String path = "C:\\Users\\nyati\\OneDrive\\Desktop\\program practice\\D2c\\";
		File file = new File(path);

		File[] files = file.listFiles();
		System.out.println("File NAme\tFile Extention\t\tModified Date");

		for (File file1 : files) {
			String f_name = file1.getName();
			int index = f_name.lastIndexOf('.');
			long time = file1.lastModified();
			DateFormat simpledateformate = new SimpleDateFormat("dd mm, yyyy");
			if (index > 0) {
				String extention = f_name.substring(index + 1, f_name.length());
				System.out.println(f_name + " \t  || \t" + extention + "\t||\t" + simpledateformate.format(time));
			}
		}

	}

}
