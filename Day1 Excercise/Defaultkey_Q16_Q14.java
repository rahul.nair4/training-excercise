interface Inter1 {
	void call();

	default void show() {
		System.out.println("Interface 1 show()");
	}
}

class AA implements Inter1 {
	public void call() {
		System.out.println("implemntation provided to call()");
		Inter1.super.show();
	}
}

public class Defaultkey_Q16_Q14 {

	public static void main(String[] args) {
		AA a = new AA();
		a.call();

	}

}
