import java.util.HashMap;

class Methods {
	int x;

	Methods(int x) {
		this.x = x;
	}

	public String toString() {
		return "" + x;
	}

	public int hashCode() {
		return x;
	}

	public boolean equals(Object O) {
		return true;
	}
}

public class HashcodeEquals {

	public static void main(String[] args) {
		HashMap value = new HashMap();

		value.put(new Methods(101), "aaa");
		value.put(new Methods(102), "bbb");
		value.put(new Methods(103), "ccc");
		value.put(new Methods(101), "AAA");

		System.out.println(value);
		;

	}

}
