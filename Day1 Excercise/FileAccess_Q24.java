import java.io.File;

public class FileAccess_Q24 {

	public static void main(String[] args) {
		String path = "D:\\Eclipse-workspace\\Excercise Day1\\src\\Circle.java";
		File file = new File(path);

		if (file.canRead())
			System.out.println("The file " + file.getName() + " has the read Access.");
		if (file.canWrite())
			System.out.print("The file " + file.getName() + " has the write Access.");

	}

	/*
	 * private static boolean isReadable(String string) { // TODO Auto-generated
	 * method stub return false; }
	 */

}
