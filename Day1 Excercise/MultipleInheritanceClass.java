/*
 * class A{ void show(){ System.out.println("Class A method Show"); } }
 * 
 * class B{ void show(){ System.out.println("Class B method Show"); } }
 * 
 * public class MultipleInheritanceClass extends A,B {
 * 
 * public static void main(String[] args) { // TODO Auto-generated method stub
 * MultipleInheritanceClass MIC=new MultipleInheritanceClass(); MIC.show();
 * 
 * }
 * 
 * }
 */
/*Question3*/

interface interface1 {
	default void show() {
		System.out.println("Interface 1 show()");
	}
}

interface interface2 {
	default void show() {
		System.out.println("Interface 2 show()");
	}
}

class A implements interface1, interface2 {
	public void show() {
		interface2.super.show();
		System.out.println(".................");
		interface1.super.show();
	}
}

class MultipleInheritanceClass {
	public static void main(String ar[]) {
		A a = new A();
		a.show();
	}
}