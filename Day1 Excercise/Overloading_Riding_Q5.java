
class Overloading {
	void Show() {
		System.out.println("Show() with no parameters");
	}

	void Show(int x, int y) {
		System.out.println("Show() with sum of two no:-" + (x + y));
	}

	void Show(int x, int y, int z) {
		System.out.println("Show() with sum of three no:-" + (x + y + z));
	}
}

public class Overloading_Riding_Q5 extends Overloading {

	void Show(int x, int y, int z) {
		System.out.println("overided Show() with multiplication of three no:-" + (x * y * z));
	}

	public static void main(String[] args) {
		Overloading_Riding_Q5 overriding = new Overloading_Riding_Q5();
		overriding.Show(3, 3, 3);
		overriding.Show(3, 3);
	}

}
