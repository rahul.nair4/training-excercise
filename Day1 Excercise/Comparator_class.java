import java.util.Comparator;
import java.util.TreeSet;

class Employe implements Comparator<StringBuffer> {

	/*
	 * public int compare(StringBuffer string1, StringBuffer string2) { String
	 * string_obj1 = string1.toString(); String string_obj2 = string2.toString();
	 * return string_obj1.compareTo(string_obj2); }
	 */

	public int compare(StringBuffer string1, StringBuffer string2) {
		String string_obj1 = string1.toString();
		String string_obj2 = string2.toString();
		return string_obj1.compareTo(string_obj2);
	}

}

public class Comparator_class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeSet idtree = new TreeSet(new Employe());

		idtree.add(new StringBuffer("10"));
		idtree.add(new StringBuffer("5"));
		idtree.add(new StringBuffer("15"));
		idtree.add(new StringBuffer(2));
		idtree.add(new StringBuffer(1));

		System.out.println(idtree);

	}

}
