import java.io.File;

public class FileorDirectory_Q25 {

	public static void main(String[] args) {
		String path = "D:\\Eclipse-workspace\\Excercise Day1\\src\\Circle.java";
		File file = new File(path);

		if (file.isFile())
			System.out.print("The given Path name is of a File i.e: " + file.getName());
		if (file.isDirectory())
			System.out.print("The given Path name is of a Directory i.e: " + file.getName());

	}

}
