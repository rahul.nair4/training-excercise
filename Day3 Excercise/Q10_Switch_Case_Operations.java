package com.Day3;

import java.util.Scanner;

public class Q10_Switch_Case_Operations {

	static void switch_cases(int first_no, int second_no) {

		while (true) {
			System.out.println("\t\t\t----Data Menu----");
			System.out.println("Enter * for :Multiplication(*) ");
			System.out.println("Enter + for :Addition(*)");
			System.out.println("Enter / for :division(/)");
			System.out.println("Enter - for :subtraction(-)");
			System.out.println("Enter Q for : Exit");
			char case_no = scan_no.next().charAt(0);
			switch (case_no) {
			case '*':
				System.out.println("Multiplication=" + first_no * second_no);
				break;
			case '+':
				System.out.println("Sum=" + (first_no + second_no));
				break;
			case '/':
				System.out.println("Division=" + first_no / second_no);
				break;
			case '-':
				System.out.println("SUbtraction=" + (first_no - second_no));
				break;
			case 'Q':
				System.exit(0);
			}
		}

	}

	static Scanner scan_no = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Enter the no to perform operations:");
		int first_no = scan_no.nextInt();
		int second_no = scan_no.nextInt();

		switch_cases(first_no, second_no);

	}

}
