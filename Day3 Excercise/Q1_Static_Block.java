package com.Day3;

class Blocks {
	static {
		System.out.println("Static block");
	}

	Blocks() {
		System.out.println("class Blocks constructor");
	}
	
	{
		System.out.println("instance block");
	}
}

public class Q1_Static_Block {

	public static void main(String[] args) {
		new Blocks();

	}

}
