package com.Day3;

enum Directions {
	NORTH, SOUTH, EAST, WEST
}

class Listof_states {
	Directions directions;

	// Constructor
	Listof_states(Directions directions) {
		this.directions = directions;
	}

	void Switch_direction() {
		switch (directions) {
		case NORTH:
			System.out.println("Haryana, Himachal Pradesh, Punjab and Rajasthan and Union Territories of Chandigarh,"
					+ " Delhi, Jammu and Kashmir and Ladakh.");
			break;
		case SOUTH:
			System.out.println(" Andhra Pradesh, Karnataka, Kerala, Tamil Nadu, and Telangana,"
					+ "as well as the union territories of Lakshadweep and Puducherry");
			break;
		case EAST:
			System.out.println("Andaman and Nicobar Islands,Bihar,Jharkhand,Odisha,West Bengal,Kolkata");
			break;
		case WEST:
			System.out.println("f Goa, Gujarat, and Maharashtra along with the Union territory of Dadra and "
					+ "Nagar Haveli and Daman and Diu,.");
			break;
		default:
			System.exit(0);
		}
	}

}

public class Q3_ENUM {
	public static void main(String[] args) {
		String direction = "NORTw";
		Listof_states t1 = new Listof_states(Directions.valueOf(direction));
		t1.Switch_direction();
	}

}
