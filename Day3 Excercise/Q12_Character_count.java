package com.Day3;

import java.util.HashMap;

public class Q12_Character_count {

	public static void main(String[] args) {
		String string = "onplan";

		HashMap<Character, Integer> map = new HashMap<>();
		int count = 0;
		for (int i = 0; i < string.length(); i++) {
			// System.out.println(string.charAt(i)+" "+i);
			char key = string.charAt(i);
			if (map.containsKey(key)) {
				int char_value = map.get(key);
				map.put(key, ++char_value);
			} else {
				map.put(key, 1);
			}

		}
		System.out.println(map);

	}

}
