package com.Day3;

import java.util.Scanner;
class InvalidAgeException extends RuntimeException
{
	InvalidAgeException()
	{}
	InvalidAgeException(String S)
	{
	super(S);	
	}
}
public class Q8_Custom_Exception {
	
	
	static void vote (int age){
		if(age<18)
		{
			throw new InvalidAgeException("You are not eligible ,Age is Invalid");
		}
		else
		{
			System.out.print("Welcome");
		}
		
	}

	public static void main(String[] args) {
		Scanner scan_age = new Scanner(System.in);
		System.out.print("Enter the Age:");
		int age=scan_age.nextInt();
//		Integer.parseInt(args[0]);
		
		vote(age);

	}

}
